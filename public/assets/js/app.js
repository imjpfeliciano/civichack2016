//creamos el modulo ferreteria
var civicApp = angular.module('civicApp', ['ngRoute']);

civicApp.config(function($routeProvider){
	$routeProvider
	.when('/', {
		templateUrl : 'public/views/home.html',
		controller : 'homeCtrl'
	})

	.otherwise({
		templateUrl : 'public/views/home.html',
	});
});

civicApp.controller('tabsCtrl', function($scope, $location) {
	$scope.isActive = function(viewLocation){
		return viewLocation === $location.path();
	}
});

civicApp.controller('homeCtrl', function ($scope, $http) {
	$scope.title = 'Home';

	$http.get('/public/resources/ContratosAñoTipo.json')
	.then(function(response) {
		$scope.dict = response.data;	//Simulamos el get de cada uno de los ficheros

		$scope.verificaAnomalias = function(params) {
			var valmin = Math.min(params[2015].amount, params[2016].amount);
			var valmax = Math.max(params[2015].amount, params[2016].amount);

			var esperado = valmin * 1.1;
			//console.log({esperado: esperado, valmax: valmax});
			//debugger;
			return !(valmax <= esperado);
		};

		//Agrupamos los elementos recibidos por las peticiones de acuerdo a la etiqueta y años
		$scope.agruparInformacionPorContratos = function(params){
			var contratos = {};

			for(var year in params){
				for(var labelContrato in params[year]){
					if(contratos[labelContrato] != undefined){
						contratos[labelContrato][year] = {
							amount: params[year][labelContrato].amount
						}
						contratos[labelContrato].showButton = $scope.verificaAnomalias(contratos[labelContrato]);
					} else {
						contratos[labelContrato] = {};

						contratos[labelContrato].description = labelContrato;
						contratos[labelContrato].info = {};

						//debugger;
						contratos[labelContrato][year] = {
    						amount: params[year][labelContrato].amount
						}
					}
				}
			}

			$scope.contratos = contratos;
		};


		$scope.datosAgrupados = $scope.agruparInformacionPorContratos(response.data);
	});



	//$scope.dict = '/public/resources/ContratosAñoTipo.json';

	
	/*
	var jsondatos = [];
	$scope.dict = {};

	currentYear = new Date().getFullYear();
	//console.log(currentYear);

	//obtenemos los contratos desde 2015 por anio
	for(var year = 2015; year <= currentYear; year++){
		$http.get('http://www.contratosabiertos.cdmx.gob.mx/api/contratos/ejercicio/' + year)
		.then(function(response){
			jsondatos[response.data[0].ejercicio] = response.data;

			for(var j = 0; j < response.data.length; j++) {
				$http.get(response.data[j].uri)
				.then(function(res) {
					var data = res.data.releases[0].contracts[0];
					//debugger;
					var dyear = res.data.releases[0].awards[0].date.slice(0,4);
					if($scope.dict[dyear] != undefined){
						//TODO
					} else {
						$scope.dict[dyear] = {};
					}
					if($scope.dict[dyear][data.description] != undefined){
						$scope.dict[dyear][data.description].amount += data.value.amount;
					} else {
						$scope.dict[dyear][data.description] = {
							year: dyear,
							description: data.description,
							amount: data.value.amount,
							currency: data.value.currency
						};
					}
				});
			}

		});
	}
	
	/*
	$scope.$on('$viewContentLoaded', function(){
		$scope.dict = dict;
	});
	*/
});