var express = require('express');
var app = express();
var port = 8000;

app.use('/public', express.static(__dirname + '/public'));
app.get('/', function(req, res){
	res.sendfile('index.html');
})

app.listen(port, function() {
	console.log('app listening on port: ' + port);
});